# TiagOS

An interactive OS-like web page where you can play around and view some files.

Created with Create-React-App.

## Tech Stack

- React w/ Typescript
- Styled-components
- Redux
- React-router-dom (v6 or above)

### Libraries

- Lodash
- Moment
- BacalhaUI (my creation)
