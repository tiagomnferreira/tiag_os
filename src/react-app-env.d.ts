/// <reference types="react-scripts" />
import "styled-components";
import { Theme } from "bacalhaui";

declare module "styled-components" {
  export interface DefaultTheme extends Theme {} // extends the global DefaultTheme with our ThemeType.
}
