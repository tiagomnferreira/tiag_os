import { Modal } from "bacalhaui";
import styled from "styled-components";

export const Container = styled(Modal)`
  box-shadow: none;
  border: ${({ theme }) => `2px solid ${theme.colors.neutral[9]}`};
  white-space: break-spaces;
`;
