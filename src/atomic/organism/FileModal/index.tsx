import { ModalProps } from "bacalhaui";
import { PropsWithChildren } from "react";
import ModalHeader from "../../molecules/ModalHeader";
import { Container } from "./styled-components";
import ModalFooter from "../../molecules/ModalFooter";

const FileModal = (props: PropsWithChildren<ModalProps>) => (
  <Container
    customFooter={<ModalFooter />}
    customHeader={<ModalHeader onClose={props.onClose} title={props.title} />}
    animations={false}
    overlayStyle={{ position: "absolute" }}
    onOverlayClick={props.onClose}
    {...props}
  >
    {props.children}
  </Container>
);

export default FileModal;
