import { memo } from "react";
import StartMenuHeader from "../../molecules/StartMenuHeader";
import { Container } from "./styled-components";

const StartMenu = () => (
  <Container>
    <StartMenuHeader />
  </Container>
);

export default memo(StartMenu);
