import styled from "styled-components";

export const Container = styled.section`
  border: 2px solid ${({ theme }) => theme.colors.neutral[9]};
  border-left: 0;
`;
