import { memo } from "react";
import DesktopMenuTrigger from "../../atoms/DesktopMenuTrigger";
import Clock from "../../components/Clock";
import Toolbar from "../../molecules/Toolbar";
import { Container } from "./styled-components";

const DesktopFooter = () => (
  <Container>
    <DesktopMenuTrigger />
    <Toolbar />
    <Clock />
  </Container>
);

export default memo(DesktopFooter);
