import styled from "styled-components";

export const Container = styled.footer`
  height: ${({ theme }) => theme.sizes[56]};
  display: flex;
  border-top: 1px solid;
  background-color: ${({ theme }) => theme.colors.red[7]};
`;
