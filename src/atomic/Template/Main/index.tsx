import { PropsWithChildren } from "react";
import { Container } from "./styled-components";

const Main = ({ children }: PropsWithChildren<unknown>) => (
  <Container>{children}</Container>
);

export default Main;
