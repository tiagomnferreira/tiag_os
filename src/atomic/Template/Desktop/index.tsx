import Footer from "../../organism/DesktopFooter";
import Main from "../Main";
import { Container } from "./styled-components";
import DesktopFilesystem from "../../components/DesktopFilesystem";

const Desktop = () => (
  <Container>
    <Main>
      <DesktopFilesystem />
    </Main>
    <Footer />
  </Container>
);

export default Desktop;
