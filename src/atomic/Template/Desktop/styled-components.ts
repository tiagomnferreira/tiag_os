import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  background-image: url("https://www.pngmart.com/files/21/90s-Logo-PNG.png");
  background-repeat: no-repeat;
  background-position: center;
  background-color: ${({ theme }) => theme.colors.yellow[1]};
`;
