import styled from "styled-components";
import { Props } from "./types";

export const Container = styled.div<Pick<Props, "direction">>`
  display: flex;
  flex-direction: ${({ direction }) => direction};
  gap: ${({ theme }) => theme.spacings[4]};
  user-select: none;
  flex-wrap: wrap;
  align-content: flex-start;
  height: 100%;
`;
