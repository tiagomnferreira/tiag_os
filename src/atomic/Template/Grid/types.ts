import { NullaryFn } from "bacalhaui";
import { Direction } from "../../../typescript/app";

export interface Props {
  onContextMenu?: NullaryFn<void>;
  direction?: Direction;
}
