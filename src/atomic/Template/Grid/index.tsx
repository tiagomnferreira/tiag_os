import { PropsWithChildren } from "react";
import { Container } from "./styled-components";
import { Props } from "./types";

const Grid = ({
  children,
  onContextMenu,
  direction = "column",
}: PropsWithChildren<Props>) => (
  <Container direction={direction} onContextMenu={onContextMenu}>
    {children}
  </Container>
);

export default Grid;
