import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  background-image: url("https://freesvg.org/img/1575010855applebite.png");
  background-size: 15%;
  background-repeat: no-repeat;
  background-position: center;
  background-color: ${({ theme }) => theme.colors.yellow[0]};
`;
