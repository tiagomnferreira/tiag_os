import { Paragraph } from "bacalhaui";
import MobileClock from "../../components/MobileClock";
import MobileFooter from "../../molecules/MobileFooter";
import WorkInProgress from "../../molecules/WorkInProgress";
import Main from "../Main";
import { Container } from "./styled-components";
import { useTheme } from "styled-components";

const Mobile = () => {
  const theme = useTheme();

  return (
    <Container>
      <Main>
        <MobileClock />
        <WorkInProgress />
        <Paragraph textAlign="center" fontSize={theme.sizes[8]}>
          Please switch back to desktop view
        </Paragraph>
      </Main>
      <MobileFooter />
    </Container>
  );
};

export default Mobile;
