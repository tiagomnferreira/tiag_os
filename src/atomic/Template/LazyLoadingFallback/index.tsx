import { PropsWithChildren, Suspense, memo } from "react";
import Loading from "../../atoms/Loading";

const LazyLoadingFallback = ({ children }: PropsWithChildren<unknown>) => (
  <Suspense fallback={<Loading />}>{children}</Suspense>
);

export default memo(LazyLoadingFallback);
