import { ChangeEvent } from "react";
import { UnaryFn } from "../../../typescript/app";

export interface Props {
  href: string;
  handleInput: UnaryFn<
    ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    void
  >;
  to: string;
  body: string;
}
