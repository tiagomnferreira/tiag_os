import { memo } from "react";
import { Container, Textarea } from "./styled-components";
import Input from "../../atoms/Input";
import Button from "../../atoms/Button";
import { Paragraph } from "bacalhaui";
import { Props } from "./types";

const EmailUI = ({ href, handleInput, body, to }: Props) => (
  <Container>
    <Paragraph>To:</Paragraph>
    <Input width="100%" name="to" onChange={handleInput} value={to} />
    <Textarea rows={15} name="body" onChange={handleInput} value={body} />
    <a href={href} target="_blank" rel="noreferrer">
      <Button width="150px">Open Email App</Button>
    </a>
  </Container>
);

export default memo(EmailUI);
