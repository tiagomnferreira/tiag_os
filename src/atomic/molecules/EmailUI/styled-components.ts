import styled from "styled-components";

export const Container = styled.div`
  display: grid;
  grid-template-columns: ${({ theme }) => `${theme.spacings[52]} 1fr`};
  gap: 8px;
  align-items: center;

  textarea {
    grid-column: span 2;
    font-family: inherit;
  }

  > :last-child {
    grid-column: 2;
    justify-self: flex-end;
  }
`;

export const Textarea = styled.textarea`
  border: 2px solid ${({ theme }) => theme.colors.neutral[9]};
`;
