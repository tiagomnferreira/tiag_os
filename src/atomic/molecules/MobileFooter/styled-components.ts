import styled from "styled-components";

export const Container = styled.footer`
  margin: 0 auto;
  padding: ${({ theme }) => theme.spacings[16]};
  border: 1px solid;
  min-width: ${({ theme }) => theme.sizes[152]};
  display: flex;
  justify-content: space-evenly;
  border-bottom: 0;
`;
