import { memo } from "react";
import { Container } from "./styled-components";
import { Icon } from "bacalhaui";

const MobileFooter = () => (
  <Container>
    <Icon name="menu" />
  </Container>
);

export default memo(MobileFooter);
