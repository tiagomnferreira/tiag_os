import { memo } from "react";
import { Props } from "./types";
import CloseButton from "../../atoms/CloseButton";
import { Container, Title } from "./styled-components";

const ModalHeader = ({ onClose, title }: Props) => (
  <Container>
    <Title textAlign="center">{title}</Title>
    <CloseButton onClose={onClose} />
  </Container>
);

export default memo(ModalHeader);
