import { Paragraph } from "bacalhaui";
import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  justify-content: flex-end;
  border-bottom: ${({ theme }) => `2px solid ${theme.colors.neutral[9]}`};
  background-color: ${({ theme }) => theme.colors.red[7]};
`;

export const Title = styled(Paragraph)`
  flex: 1;
`;
