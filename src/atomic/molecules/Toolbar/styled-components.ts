import styled from "styled-components";

export const Container = styled.div`
  flex-grow: 1;
  border-right: 1px solid;
  border-left: 1px solid;
  display: flex;
  align-items: center;
  padding: ${({ theme }) => theme.spacings[4]};
`;
