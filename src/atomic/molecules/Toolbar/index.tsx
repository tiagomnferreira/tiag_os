import { memo } from "react";
import { Container } from "./styled-components";
import EmailApp from "../../components/EmailApp";
import ToolbarOpenPaths from "../../components/ToolbarOpenPaths";

const Toolbar = () => (
  <Container>
    <EmailApp />
    <ToolbarOpenPaths />
  </Container>
);

export default memo(Toolbar);
