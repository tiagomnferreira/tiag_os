import styled from "styled-components";

export const Container = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: ${({ theme }) => theme.spacings[32]};
  height: ${({ theme }) => theme.sizes[104]};
`;
