import { Paragraph } from "bacalhaui";
import Loading from "../../atoms/Loading";
import { memo } from "react";
import { Container } from "./styled-components";

const WorkInProgress = () => (
  <Container>
    <Loading />
    <Paragraph>Work in progress...</Paragraph>
  </Container>
);

export default memo(WorkInProgress);
