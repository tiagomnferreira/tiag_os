import { Container } from "./styled-components";
import { Props } from "./types";

const ClockDisplay = ({ time, fontSize }: Props) => (
  <Container fontSize={fontSize}>{time}</Container>
);

export default ClockDisplay;
