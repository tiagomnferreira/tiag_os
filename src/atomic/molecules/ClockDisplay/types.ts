import { ParagraphProps } from "bacalhaui";

export interface Props extends Pick<ParagraphProps, "fontSize"> {
  time: string;
}
