import { Paragraph } from "bacalhaui";
import styled from "styled-components";

export const Container = styled(Paragraph)`
  padding: ${({ theme }) => `0 ${theme.spacings[12]}`};
  display: flex;
  align-items: center;
`;
