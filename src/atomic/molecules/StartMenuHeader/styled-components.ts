import { Avatar } from "bacalhaui";
import styled from "styled-components";

export const Container = styled.div`
  background-color: ${({ theme }) => theme.colors.red[7]};
  display: flex;
`;

export const UserAvatar = styled(Avatar)`
  background-color: ${({ theme }) => theme.colors.yellow[9]};
  border-right: 2px solid ${({ theme }) => theme.colors.neutral[9]};
  border-radius: 0;
`;

export const Info = styled.article`
  justify-content: center;
  display: flex;
  flex-direction: column;
  padding: ${({ theme }) => theme.spacings[4]};
  flex: 1;
`;

export const Corner = styled.div`
  background-color: ${({ theme }) => theme.colors.yellow[9]};
  border-left: 2px solid ${({ theme }) => theme.colors.neutral[9]};
  width: ${({ theme }) => theme.sizes[64]};
`;
