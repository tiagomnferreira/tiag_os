import { memo } from "react";
import { Container, Corner, Info, UserAvatar } from "./styled-components";
import { Paragraph } from "bacalhaui";
import { useTheme } from "styled-components";

const StartMenuHeader = () => {
  const theme = useTheme();

  return (
    <Container>
      <UserAvatar
        borderless
        src="https://media.licdn.com/dms/image/C4D03AQE0BO2sx6b6Ag/profile-displayphoto-shrink_800_800/0/1539342281831?e=1684972800&v=beta&t=aRqFdc06ZqN2O1OoGsv_92ZRF1Wy7u3yN9K5mQT7LEE"
      />
      <Info>
        <Paragraph fontSize={theme.sizes[24]}>Tiago Ferreira</Paragraph>
        <Paragraph>Senior Software Engineer</Paragraph>
      </Info>
      <Corner />
    </Container>
  );
};

export default memo(StartMenuHeader);
