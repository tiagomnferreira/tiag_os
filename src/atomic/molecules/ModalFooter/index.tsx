import { PropsWithChildren, memo } from "react";
import { Container } from "./styled-components";

const ModalFooter = (props: PropsWithChildren<unknown>) => (
  <Container>{props.children}</Container>
);

export default memo(ModalFooter);
