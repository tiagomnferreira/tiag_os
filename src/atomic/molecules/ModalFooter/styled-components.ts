import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  border-top: ${({ theme }) => `2px solid ${theme.colors.neutral[9]}`};
  background-color: ${({ theme }) => theme.colors.red[7]};
  min-height: ${({ theme }) => theme.sizes[24]};
`;
