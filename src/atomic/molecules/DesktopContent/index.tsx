import { getIconNameForFile } from "../../../utils/functions";
import Grid from "../../Template/Grid";
import HomeIcon from "../../atoms/DesktopIcon";
import { Props } from "./types";

const DesktopContent = ({
  filesystem,
  onFileDoubleClick = () => null,
  onContextMenu,
  direction,
  onFileClick = () => null,
  labeledContent = true,
}: Props) => (
  <Grid direction={direction} onContextMenu={onContextMenu}>
    {filesystem.map((file, index) => (
      <HomeIcon
        key={file.path + index}
        onDoubleClick={() => onFileDoubleClick(file)}
        label={labeledContent ? file.name : ""}
        name={getIconNameForFile(file) as any}
        onClick={() => onFileClick(file)}
      />
    ))}
  </Grid>
);

export default DesktopContent;
