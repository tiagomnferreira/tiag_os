import { NullaryFn, UnaryFn } from "bacalhaui";
import { File } from "../../../typescript/models";
import { Direction } from "../../../typescript/app";

export interface Props {
  filesystem: File[];
  onFileDoubleClick?: UnaryFn<File, void>;
  onFileClick?: UnaryFn<File, void>;
  onContextMenu?: NullaryFn<void>;
  direction?: Direction;
  labeledContent?: boolean;
}
