import { lazy, memo } from "react";
import useBreakpoint from "../../../hooks/useBreakpoint";
import LazyLoadingFallback from "../../Template/LazyLoadingFallback";

const MobilePage = lazy(() => import("../MobilePage"));
const DesktopPage = lazy(() => import("../DesktopPage"));

const ViewHandler = () => {
  const { isSmallDevice } = useBreakpoint();

  if (isSmallDevice)
    return (
      <LazyLoadingFallback>
        <MobilePage />
      </LazyLoadingFallback>
    );

  return (
    <LazyLoadingFallback>
      <DesktopPage />
    </LazyLoadingFallback>
  );
};

export default memo(ViewHandler);
