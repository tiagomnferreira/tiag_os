import { memo } from "react";
import Desktop from "../../Template/Desktop";

const DesktopPage = () => {
  return <Desktop />;
};

export default memo(DesktopPage);
