import { memo } from "react";
import Mobile from "../../Template/Mobile";

const MobilePage = () => <Mobile />;

export default memo(MobilePage);
