import { ComponentVariants } from "bacalhaui";
import { Icon } from "../Icon";
import { memo } from "react";
import { useTheme } from "styled-components";
import { Props } from "./types";
import { Container } from "./styled-components";

const CloseButton = ({ onClose }: Props) => {
  const theme = useTheme();
  return (
    <Container
      width={theme.sizes[24]}
      height={theme.sizes[24]}
      onClick={onClose}
      variant={ComponentVariants.SECONDARY}
    >
      <Icon.Close fill={theme.colors.neutral[8]} />
    </Container>
  );
};

export default memo(CloseButton);
