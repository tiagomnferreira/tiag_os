import { NullaryFn } from "../../../typescript/app";

export interface Props {
  onClose: NullaryFn<void>;
}
