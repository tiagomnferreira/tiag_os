import { Button } from "bacalhaui";
import styled from "styled-components";

export const Container = styled(Button)`
  display: flex;
  align-items: center;
  border-left: ${({ theme }) => `2px solid ${theme.colors.neutral[7]}`};
  border-radius: 0;
  padding: 0;

  :hover {
    box-shadow: none;
  }
`;
