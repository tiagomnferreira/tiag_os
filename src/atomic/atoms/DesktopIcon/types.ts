import { IconProps } from "bacalhaui";
import { NullaryFn } from "../../../typescript/app";

export interface Props extends IconProps {
  label?: string;
  onDoubleClick?: NullaryFn<void>;
  onClick?: NullaryFn<void>;
}
