import styled, { css } from "styled-components";
import { Props } from "./types";
import { adjustColorOpacity } from "bacalhaui";

export const Container = styled.div<Pick<Props, "label">>`
  padding: ${({ theme }) => theme.spacings[8]};
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  fill: ${({ theme }) => theme.colors.neutral[7]};
  color: ${({ theme }) => theme.colors.neutral[7]};
  font-weight: bold;
  width: ${({ theme }) => theme.sizes[80]};
  overflow-wrap: break-word;
  box-sizing: border-box;
  height: 100%;
  max-height: ${({ theme }) => theme.sizes[88]};
  width: ${({ theme }) => theme.sizes[88]};

  :hover {
    cursor: pointer;
    background-color: ${adjustColorOpacity("red", "2", 50)};
  }

  :active {
    background-color: ${adjustColorOpacity("red", "2", 75)};
  }

  ${({ label }) =>
    label &&
    css`
      ::after {
        content: "${label}";
        font-size: ${({ theme }) => theme.sizes[8]};
        width: 100%;
        text-align: center;
        margin-top: ${({ theme }) => theme.spacings[8]};
      }
    `};
`;
