import { Icon, SizeVariants } from "bacalhaui";
import { Container } from "./styled-components";
import { Props } from "./types";

const DesktopIcon = ({ name, label, onDoubleClick, onClick }: Props) => (
  <Container label={label} onDoubleClick={onDoubleClick} onClick={onClick}>
    <Icon name={name} size={SizeVariants.LG} />
  </Container>
);

export default DesktopIcon;
