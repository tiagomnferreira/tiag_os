import { InputProps } from "bacalhaui";
import { Container } from "./styled-components";

const Input = (props: InputProps) => <Container borderless {...props} />;

export default Input;
