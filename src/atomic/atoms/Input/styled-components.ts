import { Input } from "bacalhaui";
import styled from "styled-components";

export const Container = styled(Input)`
  border: 2px solid ${({ theme }) => theme.colors.neutral[9]};
  border-radius: 0;
  input {
    font-family: inherit;
  }
`;
