import { memo } from "react";
import { Worker } from "@react-pdf-viewer/core";
// Import the main component
import { Viewer } from "@react-pdf-viewer/core";
// Import the styles
import "@react-pdf-viewer/core/lib/styles/index.css";
import { base4 } from "./pdfBase64";
import { base64toBlob } from "./utils";

// `base64String` is the given base 64 data
const blob = base64toBlob(base4);
const url = URL.createObjectURL(blob);

const PDFViewer = () => (
  <Worker workerUrl="https://unpkg.com/pdfjs-dist@3.3.122/build/pdf.worker.min.js">
    <Viewer fileUrl={url} />
  </Worker>
);

export default memo(PDFViewer);
