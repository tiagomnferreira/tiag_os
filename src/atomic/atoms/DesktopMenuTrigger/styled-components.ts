import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${({ theme }) => theme.sizes[64]};
  height: ${({ theme }) => theme.sizes[56]};

  :hover {
    cursor: pointer;
  }
`;
