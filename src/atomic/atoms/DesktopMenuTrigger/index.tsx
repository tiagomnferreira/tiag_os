import { Dropdown, Icon } from "bacalhaui";
import { Container } from "./styled-components";
import StartMenu from "../../organism/StartMenu";
import { memo } from "react";

const DesktopMenuTrigger = () => (
  <Dropdown
    content={<StartMenu />}
    position="top-left"
    clickAndClose={false}
    noStyleDropdown
  >
    <Container>
      <Icon name="menu" />
    </Container>
  </Dropdown>
);

export default memo(DesktopMenuTrigger);
