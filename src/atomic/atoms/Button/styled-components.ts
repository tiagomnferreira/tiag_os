import { Button } from "bacalhaui";
import styled from "styled-components";

export const Container = styled(Button)`
  font-family: inherit;
  background-color: ${({ theme }) => theme.colors.purple[5]};
  border-radius: 0;
  border: 2px solid ${({ theme }) => theme.colors.neutral[9]};
  transition: none;

  :hover {
    background-color: ${({ theme }) => theme.colors.purple[6]};
  }
`;
