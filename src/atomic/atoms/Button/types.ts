import { ButtonProps } from "bacalhaui";

export interface Props extends Omit<ButtonProps, "variant"> {}
