import styled, { keyframes } from "styled-components";
import { Icon } from "../Icon";

const rotate = keyframes`
    from {
        transform: rotate(0);
    }
    to {
        transform: rotate(360deg);
    }
`;

export const Container = styled(Icon.Hourglass)`
  height: ${({ theme }) => theme.sizes[24]};
  width: ${({ theme }) => theme.sizes[24]};
  animation: ${rotate} 2s;
  animation-iteration-count: infinite;
  animation-timing-function: linear;
  margin: auto;
`;
