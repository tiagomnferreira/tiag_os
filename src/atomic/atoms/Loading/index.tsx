import { memo } from "react";
import { Container } from "./styled-components";

const Loading = () => <Container />;

export default memo(Loading);
