import { memo, useMemo } from "react";
import { useSelector } from "../../../hooks/useSelector";
import { getFilesystemByPath, getOpenFiles } from "../../../redux/selectors";
import DesktopContent from "../../molecules/DesktopContent";
import { useDispatch } from "../../../hooks/useDispatch";
import FolderViewer from "../FolderViewer";
import {
  closePath,
  minimizePath,
  openPath,
} from "../../../redux/slices/filesystem";
import { File, FileType } from "../../../typescript/models";
import { parseFilesystemToFiles } from "../../../utils/functions";
import FileViewer from "../FileViewer";

const DesktopFilesystem = () => {
  // REDUX
  const dispatch = useDispatch();
  const files = useSelector(getFilesystemByPath("/"));
  const openFiles = useSelector(getOpenFiles);

  // Orders file array so folders show up first
  const filesystem = useMemo(
    () =>
      parseFilesystemToFiles(files).sort((a, b) => (a.type > b.type ? -1 : 1)),
    [files]
  );

  const onFileDoubleClick = (payload: File) => {
    dispatch(openPath(payload));
  };

  const onFileClose = (payload: File) => {
    dispatch(closePath(payload));
  };

  const onFileMinimize = (payload: File) => {
    dispatch(minimizePath(payload));
  };

  return (
    <>
      <DesktopContent
        filesystem={filesystem}
        onFileDoubleClick={onFileDoubleClick}
        onContextMenu={() => console.log("context menu")}
      />
      {openFiles.map((file) =>
        file.type === FileType.FOLDER ? (
          <FolderViewer
            file={file}
            openPath={onFileDoubleClick}
            closePath={() => onFileClose(file)}
            minimizePath={() => onFileMinimize(file)}
          />
        ) : (
          <FileViewer
            file={file}
            closePath={() => onFileClose(file)}
            minimizePath={() => onFileMinimize(file)}
          />
        )
      )}
    </>
  );
};

export default memo(DesktopFilesystem);
