import { ChangeEvent, memo, useState } from "react";
import DesktopIcon from "../../atoms/DesktopIcon";
import FileModal from "../../organism/FileModal";
import EmailUI from "../../molecules/EmailUI";
import { useTheme } from "styled-components";
import { Data } from "./types";

const EmailApp = () => {
  const theme = useTheme();
  const [isOpen, setOpen] = useState(false);
  const [data, setData] = useState<Data>({
    to: "tiagomnferreira@gmail.com",
    body: "",
  });

  const toggleOpen = () => setOpen((prev) => !prev);

  const handleInput = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) =>
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });

  const href = `mailto:${data.to}?body=${data.body}`;

  return (
    <>
      <DesktopIcon
        name="email"
        onClick={toggleOpen}
        color={theme.colors.neutral[9]}
      />
      <FileModal
        isOpen={isOpen}
        onClose={toggleOpen}
        title="Email"
        maxHeight="initial"
      >
        <EmailUI href={href} handleInput={handleInput} {...data} />
      </FileModal>
    </>
  );
};

export default memo(EmailApp);
