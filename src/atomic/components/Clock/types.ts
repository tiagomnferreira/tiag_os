import { ParagraphProps } from "bacalhaui";

export type Props = Pick<ParagraphProps, "fontSize">;
