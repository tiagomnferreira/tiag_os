import { memo, useEffect, useState } from "react";
import ClockDisplay from "../../molecules/ClockDisplay";
import moment from "moment";
import { Props } from "./types";

const Clock = ({ fontSize }: Props) => {
  const [time, setTime] = useState(moment().local());

  useEffect(() => {
    const nextMinute = moment().endOf("minute");
    const msUntilNextMinute = nextMinute.diff(time, "ms");

    setTimeout(() => {
      setTime(moment().local());
    }, msUntilNextMinute);
  }, [time]);

  return <ClockDisplay time={time.format("HH:mm")} fontSize={fontSize} />;
};

export default memo(Clock);
