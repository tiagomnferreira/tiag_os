import { File, FileExtension } from "../../../typescript/models";
import PDFViewer from "../../atoms/PDFViewer";

export const renderContent = (file: File) => {
  switch (file.extension) {
    case FileExtension.PDF:
      return <PDFViewer />;
    case FileExtension.TEXT:
      return <div dangerouslySetInnerHTML={{ __html: file.content || "" }} />;
    default:
      return "";
  }
};
