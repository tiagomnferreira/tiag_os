import FileModal from "../../organism/FileModal";
import { renderContent } from "./renderer";
import { Props } from "./types";

const FileViewer = ({ file, closePath, minimizePath }: Props) => (
  <FileModal
    isOpen
    onClose={closePath}
    maxHeight="80%"
    maxWidth="80%"
    minHeight="80%"
    title={file.name}
    onOverlayClick={minimizePath}
  >
    {renderContent(file)}
  </FileModal>
);

export default FileViewer;
