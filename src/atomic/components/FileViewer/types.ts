import { NullaryFn, UnaryFn } from "bacalhaui";
import { File } from "../../../typescript/models";

export interface Props {
  file: File;
  closePath: NullaryFn<void>;
  minimizePath: NullaryFn<void>;
}
