import styled from "styled-components";

export const Container = styled.section`
  display: flex;
  justify-content: center;
  padding: ${({ theme }) => `${theme.spacings[32]} 0`};
`;
