import { useTheme } from "styled-components";
import Clock from "../Clock";
import { Container } from "./styled-components";
import { memo } from "react";

const MobileClock = () => {
  const theme = useTheme();
  return (
    <Container>
      <Clock fontSize={theme.sizes[96]} />
    </Container>
  );
};

export default memo(MobileClock);
