import { useSelector } from "../../../hooks/useSelector";
import { getFilesystemByPath } from "../../../redux/selectors";
import { Props } from "./types";
import { parseFilesystemToFiles } from "../../../utils/functions";
import DesktopContent from "../../molecules/DesktopContent";
import FileModal from "../../organism/FileModal";

const FolderViewer = ({ file, openPath, closePath, minimizePath }: Props) => {
  const files = useSelector(getFilesystemByPath(file.path));

  return (
    <FileModal
      isOpen
      onClose={closePath}
      title={file.name}
      onOverlayClick={minimizePath}
    >
      <DesktopContent
        onFileDoubleClick={openPath}
        filesystem={parseFilesystemToFiles(files)}
        direction="row"
      />
    </FileModal>
  );
};

export default FolderViewer;
