import { NullaryFn, UnaryFn } from "bacalhaui";
import { File } from "../../../typescript/models";

export interface Props {
  file: File;
  openPath: UnaryFn<File, void>;
  closePath: NullaryFn<void>;
  minimizePath: NullaryFn<void>;
}
