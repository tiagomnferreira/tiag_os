import { memo } from "react";
import { useSelector } from "../../../hooks/useSelector";
import { getMinimizedFiles } from "../../../redux/selectors";
import DesktopContent from "../../molecules/DesktopContent";
import { File } from "../../../typescript/models";
import { useDispatch } from "../../../hooks/useDispatch";
import { openPath } from "../../../redux/slices/filesystem";

const ToolbarOpenPaths = () => {
  const dispatch = useDispatch();
  const files = useSelector(getMinimizedFiles);

  const onFileClick = (file: File) => {
    dispatch(openPath(file));
  };

  return (
    <DesktopContent
      direction="row"
      filesystem={files}
      onFileClick={onFileClick}
      labeledContent={false}
    />
  );
};

export default memo(ToolbarOpenPaths);
