import { RouteObject } from "react-router-dom";
import App from "../App";
import { PATHS } from "./paths";
import ViewHandler from "../atomic/pages/ViewHandler";

const routes: RouteObject[] = [
  {
    path: PATHS.HOME,
    element: <App />,
    children: [{ index: true, element: <ViewHandler /> }],
  },
];

export default routes;
