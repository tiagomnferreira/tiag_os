import { IconName } from "bacalhaui/dist/cjs/atomic/atoms/Icon/types";
import { File, FileExtension, FileType } from "../typescript/models";
import { get } from "lodash";
import moment from "moment";
import { BIRTH_DAY, BIRTH_MONTH, BIRTH_YEAR } from "./constants";

export const getIconNameForFile = (file: File): IconName => {
  if (!file.extension && file.type === FileType.FOLDER) return "folder";

  switch (file.extension) {
    case FileExtension.TEXT:
      return "fileText";
    case FileExtension.PDF:
      return "filePdf";
    default:
      return "unknown";
  }
};

export const parseFilesystemToFiles = (files: File) =>
  Object.keys(files).reduce((acc: File[], key: string) => {
    const file = get(files, key, null);

    if (!file || typeof file === "string" || file.path === "/") return acc;

    return [...acc, file];
  }, []);

export const getAge = () => {
  const now = moment();
  const birthdate = moment(`${BIRTH_DAY}-${BIRTH_MONTH}-${BIRTH_YEAR}`);

  return now.diff(birthdate, "year");
};
