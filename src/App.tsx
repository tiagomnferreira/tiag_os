import { Outlet } from "react-router-dom";
import { memo } from "react";

const App = () => {
  return (
    <div
      style={{ height: "100vh", display: "flex", overflow: "hidden" }}
      onContextMenu={(e) => e.preventDefault()}
    >
      <Outlet />
    </div>
  );
};

export default memo(App);
