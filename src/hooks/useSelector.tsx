import { useSelector as useReduxSelector } from "react-redux";
import type { TypedUseSelectorHook } from "react-redux";
import type { ReduxState } from "../redux/store";

export const useSelector: TypedUseSelectorHook<ReduxState> = useReduxSelector;
