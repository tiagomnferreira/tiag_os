import { useEffect, useState } from "react";
import { throttle } from "lodash";
import { DeviceState } from "../typescript/style";
import { getBreakpoint } from "../style/utils";

function calcInnerWidth() {
  /* Get window inner width here */
}
window.addEventListener("resize", throttle(calcInnerWidth, 500));

const getDeviceByWidth = (width: number) => {
  const state: DeviceState = {
    isMobileS: false,
    isMobileM: false,
    isMobileL: false,
    isTablet: false,
    isLaptop: false,
    isLaptopL: false,
    isDesktop: false,
    isLargeDevice: false,
    isMidDevice: false,
    isSmallDevice: false,
  };

  if (width <= getBreakpoint("mobileS")) {
    state.isMobileS = true;
    state.isSmallDevice = true;
  } else if (width <= getBreakpoint("mobileM")) {
    state.isMobileM = true;
    state.isSmallDevice = true;
  } else if (width <= getBreakpoint("mobileL")) {
    state.isMobileL = true;
    state.isSmallDevice = true;
  } else if (width <= getBreakpoint("tablet")) {
    state.isTablet = true;
    state.isMidDevice = true;
  } else if (width <= getBreakpoint("laptop")) {
    state.isLaptop = true;
    state.isMidDevice = true;
  } else if (width <= getBreakpoint("laptopL")) {
    state.isLaptopL = true;
    state.isLargeDevice = true;
  } else {
    state.isDesktop = true;
    state.isLargeDevice = true;
  }

  return state;
};

const useBreakpoint = () => {
  const [device, setDevice] = useState(() =>
    getDeviceByWidth(window.innerWidth)
  );

  useEffect(() => {
    const calcInnerWidth = throttle(function () {
      setDevice(getDeviceByWidth(window.innerWidth));
    }, 200);
    window.addEventListener("resize", calcInnerWidth);
    return () => window.removeEventListener("resize", calcInnerWidth);
  }, []);

  return device;
};

export default useBreakpoint;
