import { useDispatch as useReduxDispatch } from "react-redux";
import type { ReduxDispatch } from "../redux/store";

export const useDispatch: () => ReduxDispatch = useReduxDispatch;
