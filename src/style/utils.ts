import { get } from "lodash";
import { Devices } from "../typescript/style";
import { breakpoits } from "./breakpoints";

export const getBreakpoint = (device: Devices) => get(breakpoits, device);
