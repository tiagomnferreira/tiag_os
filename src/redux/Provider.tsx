import { PropsWithChildren } from "react";
import { Provider } from "react-redux";
import store from "./store";

const ReduxProvider = (props: PropsWithChildren<unknown>) => (
  <Provider store={store}>{props.children}</Provider>
);

export default ReduxProvider;
