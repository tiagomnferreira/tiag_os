import { get } from "lodash";
import { ReduxState } from "./store";
import { File, FileType } from "../typescript/models";

export const getFilesystemByPath =
  (path: string) =>
  (state: ReduxState): File =>
    get(
      state.filesystemSlice,
      `files${
        path && path !== "/" ? path.split(".")[0].replaceAll("/", ".") : ""
      }`,
      { name: "Not found", path, type: FileType.FILE }
    );

export const getOpenFiles = (state: ReduxState): File[] =>
  state.filesystemSlice.pathsOpen
    .slice(0, 5) // We slice the array for performance reasons
    .map((path) => getFilesystemByPath(path)(state));

export const getMinimizedFiles = (state: ReduxState): File[] =>
  state.filesystemSlice.pathsRunning
    .slice(0, 5) // We slice the array for performance reasons
    .map((path) => getFilesystemByPath(path)(state));
