import { configureStore } from "@reduxjs/toolkit";
import reducers from "./slices";

const store = configureStore({
  reducer: reducers,
});

export default store;

export type ReduxState = ReturnType<typeof store.getState>;
export type ReduxDispatch = typeof store.dispatch;
