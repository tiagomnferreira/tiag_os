import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { File, FileType } from "../../typescript/models";
import { files } from "./constants";

interface State {
  pathsOpen: string[];
  files: File;
  pathsRunning: string[];
}

const initialState: State = {
  pathsOpen: [],
  pathsRunning: [],
  files,
};

const filesystemSlice = createSlice({
  name: "filesystem",
  initialState,
  reducers: {
    addFile(state, action: PayloadAction<File>) {
      const { payload } = action;
      const newFile: File = {
        ...payload,
        type: FileType.FILE,
      };

      const statePath =
        payload.path.replace("/", ".") + `.${payload.extension}`;

      state.files[statePath] = newFile;
    },
    addFolder(state, action: PayloadAction<File>) {
      const { payload } = action;
      const newFolder = {
        ...payload,
        type: FileType.FOLDER,
      };

      const statePath = payload.path.replace("/", ".");

      state.files[statePath] = newFolder;
    },
    openPath(state, action: PayloadAction<File>) {
      const { payload } = action;

      // If the path is already running, we add it to the paths opened
      if (!state.pathsRunning.includes(payload.path))
        state.pathsRunning.push(payload.path);

      if (state.pathsOpen.includes(payload.path))
        state.pathsOpen.splice(state.pathsOpen.indexOf(payload.path, 1));

      state.pathsOpen.push(payload.path);
    },
    closePath(state, action: PayloadAction<File>) {
      const { payload } = action;

      state.pathsOpen.splice(state.pathsOpen.indexOf(payload.path, 1));
      state.pathsRunning.splice(state.pathsOpen.indexOf(payload.path, 1));
    },
    minimizePath(state, action: PayloadAction<File>) {
      const { payload } = action;

      state.pathsOpen.splice(state.pathsOpen.indexOf(payload.path, 1));
      state.pathsRunning.push(
        state.pathsRunning.splice(
          state.pathsRunning.indexOf(payload.path, 1)
        )[0]
      );
    },
  },
});

const { actions, reducer } = filesystemSlice;
export const { addFile, addFolder, openPath, closePath, minimizePath } =
  actions;

export default reducer;
