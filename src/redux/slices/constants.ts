import { File, FileExtension, FileType } from "../../typescript/models";
import { getAge } from "../../utils/functions";

const ABOUT_ME = `My name is Tiago Ferreira, and I was born in Paredes, Portugal and I'm ${getAge()} years old. I spent my early teenage years in Penafiel before moving to Porto in my early 20s, where I've been living ever since.

As a Senior Software Engineer, I'm highly skilled and experienced in building web applications. When I'm not at work, I like to hit the gym every day to stay fit and healthy. I also enjoy playing video games, which I find both relaxing and mentally stimulating.

While I excel at the technical side of my job, I know that management and leadership are important skills to have in any industry. I aspire to become an effective manager who can lead and motivate teams to achieve great results. Although I don't currently possess a strong management skillset, I'm dedicated to improving in this area.

Overall, I'm a driven and talented individual who is dedicated to self-improvement and continuous development.`;

const ROADMAP = `Coming next:

- Personal projects;
- Mobile experience;
- Professional experience;`;

// const BLIP_A = `My first professional experience involved working with AngularJS and Node.js to maintain and enhance a tool used by people to request participation in workshops or request the company to buy them tickets to conferences and events for skill development.

// During my time working on the tool, I was responsible for fixing bugs, adding new features, and ensuring that everything was running smoothly. This experience gave me a strong foundation in both front-end and back-end development, and helped me develop skills in software maintenance and enhancement.

// Through my work, I learned the importance of clear communication with team members and users, as well as the significance of understanding the needs of the end-users in order to develop effective software solutions. I developed a keen eye for detail and a strong sense of problem-solving, which has proved to be invaluable in my career as a Senior Software Engineer.

// Overall, my first professional experience was a valuable learning opportunity that helped me build a solid foundation in software development and problem-solving. It was an experience that paved the way for my future successes and taught me valuable lessons that I continue to apply in my work today.`;

// const BLIP_B = `For my second professional experience, my employer challenged me to develop a tool where users could share coding tips and hints with others and where people could upvote or downvote these posts. I collaborated with two other developers to build the tool, which was developed exclusively on the front-end using React.

// This was the first time I had the opportunity to work with React, and I found the experience to be both challenging and rewarding. Working with my team, we created a user-friendly interface and implemented features that allowed users to interact with and upvote/downvote posts.

// Throughout the project, my skills in software development and problem-solving continued to grow. I gained valuable experience working collaboratively with a team and was able to apply my knowledge of AngularJS and Node.js to the new technology stack. The project also honed my skills in front-end development, particularly in React, which I found to be a powerful and flexible tool for building user interfaces.

// Overall, my second professional experience allowed me to take on a new challenge and expand my skill set in software development. It was an experience that further reinforced my passion for software development and my dedication to continuous learning and improvement.`;

// const PRIMEIT_A = `Coming soon...`;

// const PRIMEIT_B = `Coming soon...`;

// const MOZANTECH_A = `Coming soon...`;

// const MOZANTECH_B = `Coming soon...`;

// const MOZANTECH_C = `Coming soon...`;

const BACALHAUI = `I've been working on a personal components library that I'm using to improve my technical skills and to build functionality that I need for my own projects. While the library might not be polished enough for others to use right now, I'm really excited about the progress I'm making and wanted to share a bit about it.

The library is built using React, styled-components, styled-system, and Storybook, and it's been a great way for me to learn more about these technologies and to apply them in a practical way. I've been focusing on building out components that I need for my own projects, but I've also been trying to make them as flexible and reusable as possible so that I can use them in future projects as well.

One of the things I love about this library is how it's helping me to improve my technical skills. By building out components in a modular way and using technologies like styled-components and styled-system, I'm learning a lot about how to write clean and maintainable code. And by using Storybook to document and test everything, I'm also improving my testing and documentation skills.

While this library might not be ready for others to use right now, I'm really excited about the progress I'm making and the ways it's helping me to improve my technical skills. Who knows, maybe someday it'll be polished enough for others to use too!

Here are two links of interest:

Storybook:  <a target="_blank" href="https://bacalhaui.netlify.app/">https://bacalhaui.netlify.app/</a>
Repository: <a target="_blank" href="https://gitlab.com/tiagomnferreira/bacalhaui">https://gitlab.com/tiagomnferreira/bacalhaui/</a>
NPM:  <a target="_blank" href="https://www.npmjs.com/package/bacalhaui">https://www.npmjs.com/package/bacalhaui/</a>
`;

export const files = {
  cv: {
    name: "Curriculum Vitae",
    type: FileType.FILE,
    extension: FileExtension.PDF,
    path: "/cv.pdf",
  },
  about_me: {
    name: "About Me",
    type: FileType.FILE,
    extension: FileExtension.TEXT,
    path: "/about_me.txt",
    content: ABOUT_ME,
  },
  roadmap: {
    name: "Roadmap",
    type: FileType.FILE,
    extension: FileExtension.TEXT,
    path: "/roadmap.txt",
    content: ROADMAP,
  },
  personal_experience: {
    name: "Personal Experience",
    type: FileType.FOLDER,
    path: "/personal_experience",
    bacalhaui: {
      name: "BacalhaUI",
      type: FileType.FILE,
      extension: FileExtension.TEXT,
      path: "/personal_experience/bacalhaui.txt",
      content: BACALHAUI,
    },
  },
  //   professional_experience: {
  //     name: "Professional Experience",
  //     type: FileType.FOLDER,
  //     path: "/professional_experience",
  //     blipA: {
  //       name: "Blip_A",
  //       type: FileType.FILE,
  //       extension: FileExtension.TEXT,
  //       path: "/professional_experience/blipA.txt",
  //       content: BLIP_A,
  //     },
  //     blipB: {
  //       name: "Blip_B",
  //       type: FileType.FILE,
  //       extension: FileExtension.TEXT,
  //       path: "/professional_experience/blipB.txt",
  //       content: BLIP_B,
  //     },
  //     primeitA: {
  //       name: "PrimeIT_A",
  //       type: FileType.FILE,
  //       extension: FileExtension.TEXT,
  //       path: "/professional_experience/primeitA.txt",
  //       content: PRIMEIT_A,
  //     },
  //     primeitB: {
  //       name: "PrimeIT_B",
  //       type: FileType.FILE,
  //       extension: FileExtension.TEXT,
  //       path: "/professional_experience/primeitB.txt",
  //       content: PRIMEIT_B,
  //     },
  //     mozantechA: {
  //       name: "Mozantech_A",
  //       type: FileType.FILE,
  //       extension: FileExtension.TEXT,
  //       path: "/professional_experience/mozantechA.txt",
  //       content: MOZANTECH_A,
  //     },
  //     mozantechB: {
  //       name: "Mozantech_B",
  //       type: FileType.FILE,
  //       extension: FileExtension.TEXT,
  //       path: "/professional_experience/mozantechB.txt",
  //       content: MOZANTECH_B,
  //     },
  //     mozantechC: {
  //       name: "Mozantech_C",
  //       type: FileType.FILE,
  //       extension: FileExtension.TEXT,
  //       path: "/professional_experience/mozantechC.txt",
  //       content: MOZANTECH_C,
  //     },
  //   },
} as unknown as File;
