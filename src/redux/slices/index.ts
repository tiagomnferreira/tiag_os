import { combineReducers } from "@reduxjs/toolkit";
import filesystemSlice from "./filesystem";

export default combineReducers({ filesystemSlice });
