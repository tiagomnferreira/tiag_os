export type NullaryFn<R> = () => R;
export type UnaryFn<P1, R> = (param1: P1) => R;

export type Direction = "row" | "column";
