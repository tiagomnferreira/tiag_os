export enum FileType {
  FILE = "file",
  FOLDER = "folder",
}

export enum FileExtension {
  PDF = "pdf",
  TEXT = "txt",
}

export interface File {
  name: string;
  path: string;
  type: FileType;
  extension?: FileExtension;
  content?: string;
  [key: string]: File | string | undefined;
}
