export type Devices =
  | "mobileS"
  | "mobileM"
  | "mobileL"
  | "tablet"
  | "laptop"
  | "laptopL"
  | "desktop";

export interface DeviceState {
  isMobileS: boolean;
  isMobileM: boolean;
  isMobileL: boolean;
  isTablet: boolean;
  isLaptop: boolean;
  isLaptopL: boolean;
  isDesktop: boolean;
  isSmallDevice: boolean;
  isLargeDevice: boolean;
  isMidDevice: boolean;
}
